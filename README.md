
## Usage

If you're using [blitz4j](https://github.com/Netflix/blitz4j) and you want a simple default log4j.configuration, you can use the ones here.

### INFO, stdout

```
java \
-Dlog4j.configuration=https://bitbucket.org/edowd/log4j-configuration/raw/master/log4j-info-stdout.properties \
-Darchaius.configurationSource.additionalUrls=... \
-jar ...
```

### DEBUG, stdout

```
java \
-Dlog4j.configuration=https://bitbucket.org/edowd/log4j-configuration/raw/master/log4j-debug-stdout.properties \
-Darchaius.configurationSource.additionalUrls=... \
-jar ...
```

